#~/.bash_profile
export EDITOR=/usr/local/bin/vim

default_username='oliverdoetsch'

# Turn off TTY "start" and "stop" commands in all interactive shells.
# They default to C-q and C-s, Bash uses C-s to do a forward history search.
stty start ''
stty stop  ''

# keep history up to date, across sessions, in realtime
#  http://unix.stackexchange.com/a/48113
export HISTCONTROL=ignoredups:erasedups         # no duplicate entries
export HISTSIZE=100000                          # big big history (default is 500)
export HISTFILESIZE=$HISTSIZE                   # big big history
which shopt > /dev/null && shopt -s histappend  # append to history, don't overwrite


## Shortcuts
alias d="cd ~/Documents"
alias dl="cd ~/Downloads"
alias dt="cd ~/Desktop"
alias w="cd ~/Workspace"
alias vimrc="vim ~/.dotfiles/vim/.vim/vimrc"

# Easier navigation: .., ..., ~ and -
alias ..="cd .."
alias cd..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# mv, rm, cp
alias mv='mv -v'
alias rm='rm -i -v'
alias cp='cp -v'

# setting some time eaters to 127.0.0.1 :)
alias hosts='sudo $EDITOR /etc/hosts'

# reload ~/.bash_profile
alias br=". ~/.bash_profile"
alias v='vim'
alias vi='vim'
alias g='git'

## colour
export colorflag="-G"
export CLICOLOR=1
export CLICOLOR_FORCE=1
export LSCOLORS=gxfxbEaEBxxEhEhBaDaCaD

# ls options: A = include hidden (but not . or ..), F = put `/` after folders, h = byte unit suffixes
alias l="ls -AFh ${colorflag}"

# List only directories
alias lsd='ls -l ${colorflag} | grep "^d"'

# Always use color output for `ls`
alias ls="command ls ${colorflag}"


## git bash-prompt

GIT_THEME_PROMPT_DIRTY='*'

function git_prompt_info() {
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  echo -e "(${ref#refs/heads/}$(parse_git_dirty))"
}

function parse_git_dirty {
  if [[ -n $(git status -s 2> /dev/null |grep -v ^# | grep -v "working directory clean" ) ]]; then
    echo -e "$GIT_THEME_PROMPT_DIRTY"
  else
    echo -e "$GIT_THEME_PROMPT_CLEAN"
  fi
}

# building Terminal String
PS1=
PS1+="\[\033[1;33m\]\W\[\033[0m\] "
PS1+="\[\033[1;32m\]"
PS1+="\[\$(git_prompt_info)\]"
PS1+="\[\033[0m\]$ "
##
export PS1

##

## functions

# Enable tab completion for `g` by marking it as an alias for `git`
if type __git_complete &> /dev/null; then
  __git_complete g __git_main
fi;

# Create a new directory and enter it
function md() {
  mkdir -p "$@" && cd "$@"
}

# List all files, long format, colorized, permissions in octal
function la(){
  ls -l  "$@" | awk '
  {
    k=0;
    for (i=0;i<=8;i++)
      k+=((substr($1,i+2,1)~/[rwx]/) *2^(8-i));
      if (k)
        printf("%0o ",k);
        printf(" %9s  %3s %2s %5s  %6s  %s %s %s\n", $3, $6, $7, $8, $5, $9,$10, $11);
      }'
    }

# Start an HTTP server from a directory, optionally specifying the port
function server() {
      local port="${1:-8000}"
      open "http://localhost:${port}/"
      # Set the default Content-Type to `text/plain` instead of `application/octet-stream`
      # And serve everything as UTF-8 (although not technically correct, this doesn’t break anything for binary files)
      python -c $'import SimpleHTTPServer;\nmap = SimpleHTTPServer.SimpleHTTPRequestHandler.extensions_map;\nmap[""] = "text/plain";\nfor key, value in map.items():\n\tmap[key] = value + ";charset=UTF-8";\nSimpleHTTPServer.test();' "$port"
  }
